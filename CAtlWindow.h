/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CATLWINDOW_H_INC
#define CATLWINDOW_H_INC

#include <windows.h>
#include "SWC.h"


namespace gui
{

/**
@brief    Базовый класс для создания "ATL окошек"
@details  библиотека ATL.DLL предоставляет средства создания различных
          OLE окошек, вот с помощью неё данный класс и позволяет их использовать.
          например для создания WebBrowser нужно установить текст окошка в
          "Shell.Explorer"
          пример:
@code
          CAtlWindow browser;
          CCP chl(WS_OVERLAPPEDWINDOW, 0, TEXT("Shell.Explorer"), 800, 600);
          if (browser.Create())
              browser.Run(0);
@endcode
          Но правильнее будет наследоваться от CAtlWindow, модифицировать CCP
          и после Create() можно получить IWebBrowser2 с помощью GetControl()
          и затем открыть страничку с помощью Navigate() полученного интерфейса.
*/
class GUI_IMPORT CAtlWindow: public CCustomWC
{
public:
    CAtlWindow();
    virtual ~CAtlWindow();
    virtual bool Create(CCP& cp);
    /**
    @brief    Получение интерфейса подконтрольного OLE объекта
    @details  Используется сразу после Create(), ATL создаёт OLE объект по
              тексту окошка например, если текст окошка был "Shell.Explorer",
              то можно попытаться получить интерфейс IWebBrowser2.
    */
    virtual HRESULT GetControl(REFIID riid, void** ppv);
    virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
protected:
    /**
    @brief    Интерфейс IAxWinHostWindow (устанавливается в Create())
    @details  Устанавливается в CAtlWindow::Create() и остаётся доступным до
              CAtlWindow::~CAtlWindow(); если по какай то причине (например
              ATL.DLL не загружается) интерфейс не доступен, вместо создания
              окошка OLE объекта будет выводиться на холст соответствующее
              сообщение о возникшей проблеме, а работа приложения может быть
              продолжена. GetControl() в этом случае будет возвращать ошибку,
              также при вызовах Create(), будет отправляться соответствующее
              сообщение в CLog::Msg(), Create() при этом будет возвращать true.

              Крайне нежелательно вызывать lpUnknown->Release() самостоятельно,
              по крайней мере убедитесь что окошко CAtlWindow::handle()
              уничтожено, т.к. вызов lpUnknown->Release() может повлечь
              обработку сообщений и прочие побочные эффекты, что может
              привести к обращению по недействительному указателю.
    */
    IUnknown* lpUnknown;
};

} // namespace gui
#endif // CATLWINDOW_H_INC

