/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CWINDOW_H_INC
#define CWINDOW_H_INC

#include <windows.h>
#include "importdefs.h"

namespace gui
{

/**
@brief    Набор параметров для создания окошка
@details  Используется для создания windows-окошка, передаётся по "цепочке"
          перегруженных классов для того чтоб они могли модифицировать значения
          по умолчанию перед тем как управление получит gui::CWindow::Create()
          также, наиболее базовый класс должен установить правильное значение в
          lpClassName, например gui::CButton::Create() устанавливает lpClassName
          в "BUTTON" и в результате мы получаем окошко - кнопку.
          Внимание! менять значение hWndParent крайне не рекомендуется, так как
          зачастую при наполнении главного окошка элементами интерфейса этот
          параметр устанавливается единожды и для всех элементов передаётся
          один и тот же экземпляр CCP по очереди. Если вы поменяли его, то
          обязаны восстановить перед возвратом управления.
@sa       gui::CWindow::Create(), gui::CButton::Create()
*/
struct GUI_IMPORT CCP
{
    HWND hWndParent; //!< менять значение hWndParent крайне не рекомендуется
    LPCTSTR lpClassName; //!< зарегистрируйте тип перед CWindow::Create()!
    LPCTSTR lpWindowName; //!< текст виджета/заголовок (для верхнего уровня).
    DWORD dwStyle; //!< стиль виджета наподобие WS_VISIBLE, WS_CHILD
    DWORD dwExStyle; //!< расширенный стиль виджета WS_EX_***
    union {
        HMENU hMenu; //!< дескриптор меню для окошек верхнего уровня
        DWORD nID; //!< ID виджета (для программирования в стиле WinAPI)
    };
    RECT position; //!< стартовая позиция виджета (устаревший способ)

    /**
    @brief    Инициализирует CCP для дочерних окошек
    @details  Внимание! Используйте одну из CCP::set() перед вызовом Create()
              у любого виджета! Кроме тех случаев когда вы УВЕРЕНЫ в том,
              что виджет сам подготовит остальные параметры.
    */
    explicit CCP(HWND Parent)
    {
        hWndParent = Parent;
        lpClassName = NULL;
        lpWindowName = NULL;
    };

    /**
    @brief    Инициализирует CCP для окошек верхнего уровня.
    @details  Если width/height отличаются от CW_USEDEFAULT то центрируется
              в область рабочего стола по оси X/Y соответственно.
    */
    CCP (DWORD Style,
         DWORD ExStyle,
         LPCTSTR WindowText,
         int width = CW_USEDEFAULT,
         int height = CW_USEDEFAULT,
         HMENU Menu = 0)
    {
        hWndParent = NULL;
        lpClassName = NULL;
        lpWindowName = WindowText;
        dwStyle = Style;
        dwExStyle = ExStyle;
        hMenu = Menu;
        position.left = CW_USEDEFAULT;
        position.top = CW_USEDEFAULT;
        position.right = width;
        position.bottom = height;
        if (width != CW_USEDEFAULT)
        {
            int param = GetSystemMetrics(SM_CXMAXIMIZED);
            if (param > 0)
                position.left = (param - width) / 2;
        }
        if (height != CW_USEDEFAULT)
        {
            int param = GetSystemMetrics(SM_CYMAXIMIZED);
            if (param > 0)
                position.top = (param - height) / 2;
        }
    };

    /**
    @brief    Установка Стиля и текста
    */
    inline void set(DWORD Style, LPCTSTR WindowText)
    {
        set(Style, 0, WindowText, CW_USEDEFAULT, CW_USEDEFAULT,
            CW_USEDEFAULT, CW_USEDEFAULT, NULL);
    };

    /**
    @brief    Установка Стиля, текста и ID
    */
    inline void set(DWORD Style, LPCTSTR WindowText, DWORD wID)
    {
        set(Style, 0, WindowText, CW_USEDEFAULT, CW_USEDEFAULT,
            CW_USEDEFAULT, CW_USEDEFAULT, (HMENU)(LRESULT)(wID));
    };

    /**
    @brief    Установка Стилей, текста, меню (или ID) остальное по умолчанию
    */
    inline void set(DWORD Style, DWORD ExStyle,
                    LPCTSTR WindowText=NULL,
                    HMENU Menu=NULL)
    {
        set(Style, ExStyle, WindowText, CW_USEDEFAULT, CW_USEDEFAULT,
            CW_USEDEFAULT, CW_USEDEFAULT, Menu);
    };

    /**
    @brief    Установка Стилей, текста, и стартового положения
    */
    inline void set(DWORD Style, DWORD ExStyle,
                    LPCTSTR WindowText, const RECT& pos)
    {
        set(Style, ExStyle, WindowText,
            pos.left, pos.top,
            pos.right, pos.bottom, NULL);
    };

    /**
    @brief    Для случаев когда нужно полностью заполнить параметры
    */
    inline void set(DWORD Style, DWORD ExStyle,
                    LPCTSTR WindowText,
                    int  x, int  y,
                    int cx, int cy,
                    HMENU Menu=NULL)
    {
        lpWindowName = WindowText;
        dwStyle = Style;
        dwExStyle = ExStyle;
        hMenu = Menu;
        position.left = x;
        position.top = y;
        position.right = cx;
        position.bottom = cy;
    };
};


/**
@brief    Класс для определения позиционирования элементов
@details  Используется для того чтоб операция изменения размеров могла быть
          обработана всей цепочкой наследования (при желании) распространение
          вызовов идёт от верхнего к базовому. (от окна верхнего уровня)
@sa       gui::CWindow::Resize()
*/
class GUI_IMPORT WPOS: public RECT
{
public:
    int padding; //!< копируется производными WPOS, используйте с осторожностью
    int margin; //!< копируется производными WPOS, используйте с осторожностью
    /**
    @brief    Создаёт новый объект WPOS
    @details  Если wParent не равен NULL берёт параметры padding/margin из него
              Если hParent является дескриптором окошка устанавливает свои
              размеры в размеры клиентской области hParent
    @sa       CWindow::Resize()
    */
    WPOS(WPOS* wParent=0, HWND hParent=0);

    /// "отрезает" верхнюю часть своего прямоугольника
    void slice_top(int offset){bottom=top+offset;};
    /// "отрезает" нижнюю часть своего прямоугольника
    void slice_bottom(int offset){top=bottom-offset;};
    /// "отрезает" левую часть своего прямоугольника
    void slice_left(int offset){right=left+offset;};
    /// "отрезает" правую часть своего прямоугольника
    void slice_right(int offset){left=right-offset;};

    /// возвращает свою ширину
    int width() const {return right-left;};
    /// возвращает свою высоту
    int height() const {return bottom-top;};

    /**
    @brief    Устанавливает свои размеры в значения из rc
    @details  Считает что в rc.right правая координата X а не размер по оси X
    */
    void set(const RECT& rc){left=rc.left;right=rc.right;top=rc.top;bottom=rc.bottom;};

    /// сохраняет свои размеры в rc (для последующей загрузки с помощью set() например)
    void get(RECT& rc){rc.left=left;rc.top=top;rc.right=right;rc.bottom=bottom;};

    /// устанавливает размеры и позицию
    void set(int l, int t, int w, int h){left=l;top=t;right=l+w;bottom=h+t;};
    /// устанавливает правую координату X так чтоб размер по X был равен w
    void set_width(int w) {right=left+w;};
    /// устанавливает нижнюю координату Y так чтоб размер по Y был равен h
    void set_height(int h) {bottom=top+h;};

    /**
    @brief Устанавливает "клетку" по оси X
    @param full_width - полная ширина поля клеток
    @param cell_count - сколько в поле клеток
    @param n_cell - индекс клетки в координаты которой нужно "встать"
    */
    void set_cell(int full_width, int cell_count, int n_cell){
        if(m_parent && cell_count > 0 && n_cell >= 0 && n_cell <= cell_count){
            float c_w = (float)full_width / cell_count;
            left = (int)((n_cell * c_w) + padding);
            right = left + (int) c_w;
            left += margin/2;
            right -= margin/2;
        }
    };

    /**
    @brief Устанавливает следующую "клетку" по оси X
    @param cellwidth - ширина следующей клетки
    @param offset - отступ от предыдущей
    */
    WPOS& set_next_cell(int cellwidth=0, int offset=0){
        if (cellwidth==0)
            cellwidth=right-left;
        right=(left=offset+right)+cellwidth;
        return *this;
    };

    /**
    @brief Устанавливает следующую "клетку" по оси Y
    @param rowheight - высота следующей клетки
    @param offset - сколько пикселей отступить от предыдущей
    */
    WPOS& set_next_row(int rowheight=0, int offset=0){
        if (rowheight==0)
            rowheight=bottom-top;
        bottom=(top=offset+bottom)+rowheight; return *this;
    };

    /// Перемещается в центр прямоугольника с указанными сторонами
    void centerize(int w, int h){top=(h-height())/2; left=(w-width())/2;};

    /// Применяет размеры к окошку
    BOOL apply_to(HWND hWindow, bool bForce = false)
    {
        if (!bForce && m_parent == 0)
            return false;
        return SetWindowPos(hWindow, 0, left, top, width(), height(),
                SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOZORDER);
    };

private:
    /**
    @brief    Указатель на родительский WPOS
    @details  Является ещё и признаком того что данный экземпляр был создан в
              процессе обработки вложенных операций переразмещения.
              если он равен 0 - функция apply_to() ничего не совершает, что
              позволяет начинать операцию переразмещения по факту
              пользовательских действий без образования рекурсий.
    @sa       gui::CWindow::Resize()
    */
    WPOS* m_parent;
};


/**
@brief    Модальный результат диалога
@details  При использовании gui::CWindow::Run() любого класса окошка верхнего
          уровня она не возвращает управление пока модальный результат равен
          MODAL_RESULT::NONE установка его в любое другое значение вызовет
          прерывание цикла обработки сообщений в контексте этого класса.
          Внимание! если модальный результат меняется не в ответ на сообщение
          из очереди сообщений - прерывание будет отложено до первого (любого)
          сообщения в очереди. Поэтому при желании прервать модальность извне
          немедленно, имеет смысл поставить в очередь пустое сообщение.

          примечание: данный перечислитель "обёрнут" в свою область видимости
          поэтому его символы доступны только по MODAL_RESULT::
@sa       gui::CWindow::Run()
*/
typedef struct { enum {
    NONE = -1,
    OK = 0,
    CANCEL,
    YES,
    NO,
};} MODAL_RESULT;


#define SELFMSG(x,y,z)    (this->MessageProc(x,y,z))
#define SELFIMSG(x,y,z)    (inherited::MessageProc(x,y,z))
#define SELFCWMSG(x,y,z)    (CWindow::MessageProc(x,y,z))
#define SELFMSGTO(a,x,y,z)    (a::MessageProc(x,y,z))

#define GETCWINDOW(h) ((CWindow*)(::GetProp(h, TEXT("CWindow"))))
#define SETCWINDOW(h,t) (::SetProp(h, TEXT("CWindow"), (HANDLE)t))


/**
@brief    Основной класс создания окна
@details  Используется как основа для всех производных оконных классов
          содержит базовую логику обработки сообщений, трансляцию событий,
          обработку "модальности окна" и предоставляет уровень логической
          абстракции обработки сообщений (каждый оконный класс не нуждается в
          родительском для своей работы, за редким исключением)
@sa       gui::CWindow::SevMessageProc()
*/
class GUI_IMPORT CWindow: private noncopyable
{
public:
    /**
    @brief    Размеры текущего шрифта.
    @details  Размеры заглавной буквы M для текущего шрифта. Автоматически
              модифицируется по сообщению WM_SETFONT
    */
    SIZE em;
    /**
    @brief    Дескриптор "фреймового окна".
    @details  Фреймовое окно получает некоторые события, в частности
              информацию о нажатии "горячих клавиш" внутри других диалогов со
              своими акселераторами, что позволяет гибко настраивать сочетания
              клавиш, которые будут работать в зависимости от фокуса ввода.
    */
    HWND frameHandle;
    /**
    @brief    Функция-пустышка для определения нестандартных классов.
    @details  Используется при создании новых классов посредством регистрации
              функцией RegisterClassEx (для WNDCLASSEX.lpfnWndProc)
    */
    static LRESULT CALLBACK DefProc(HWND, UINT, WPARAM, LPARAM);

    ///       Конструктор
    CWindow();
    /**
    @brief    Деструктор
    @details  освобождает акселератор если установлен и уничтожает окошко после
              "отключения" от его процедуры.
    */
    virtual ~CWindow();

    /**
    @brief    Создаёт окошко по параметрам из cp
    @details  Значения класса, стилей и прочего устанавливаются цепочкой
              наследующих классов, к моменту вызова CWindow::Create() обязаны
              быть корректными. После создания окошка подменяется его процедура
              на внутреннюю функцию которая "дёргает" классы используя
              сохранённые указатели на CWindow*, сообщение WM_CREATE не
              обрабатывается (никакие сообщения не обрабатываются классами до
              момента подмены WNDPROC окошка), используйте переопределение
              Create() в ваших классах для всей необходимой инициализации.
    */
    virtual bool Create(CCP& cp);

    /**
    @brief    Подготовка к "модальности" (событие)
    @details  Вызывается автоматически из Run(). Подготавливается к отображению
              себя в виде модального диалога к примеру "отключает" предыдущее
              окошко.
    */
    virtual bool BeforeEnterMsgLoop(HWND own);
    /**
    @brief    Освобождение от "модальности" (событие)
    @details  Вызывается автоматически при завершении работы Run()
              к примеру "включает" и активизирует предыдущее окошко.
    */
    virtual bool AfterEnterMsgLoop(HWND own);

    /**
    @brief    Отображает окошко модально
    @details  Позволяет отобразить окошко верхнего уровня модально, относительно
              "hModalParent" если он указан. Если имеется родительское окно то
              оно "отключается", а это окно выводится на передний план, причем
              дальнейшая выборка сообщений уже производиться в контексте этого
              класса, до момента установки модального результата в любое
              значение кроме MODAL_RESULT::NONE.

              Также в цикле выборки сообщений происходит расширенная
              перекрываемая трансляция акселераторов:

              пример: главное окно приложения назначает Ctrl+F на функцию поиска,
              а его дочернее окно использует это же сочетание для поиска в
              тексте, следуя логике разделения дочернее окно не должно общаться
              с родителем для осуществления перехвата (разработчик его класса
              может не знать где его класс будет использоваться) логика работы:
              TranslateAccelerator вызывается для accelTable класса в котором
              был фокус ввода и по очереди для всех родительских (для WS_CHILD)
              до первой успешной трансляции, в последнюю очередь трансляция
              пробуется для CWindow::accelTable полученной из frameHandle (без
              проверок, frameHandle может быть любым окном), далее трансляция не
              происходит, таким образом можно блокировать "выход" сочетаний за
              пределы окна (например окошко ввода этих самых сочетаний CHotkey
              устанавливает frameHandle в значение равное своему handle()).
              Также трансляция прерывается на первом встретившимся WS_DISABLED
              окошке.
    @sa       SetModalResult()
    */
    virtual int Run(HWND hModalParent);

    /**
    @brief    Функция обработки сообщений
    @details  По сути - то же самое что сабклассинг оконных процедур,
              вызывается ДО обработки родной функцией окошка, позволяя
              обработать или отменить обработку (обработка совершается, только
              если управление получает CWindow::MessageProc).
    */
    virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
    /**
    @brief    Функция обработки "собственных" сообщений
    @details  В данную функцию на обработку попадают сообщения наподобие
              WM_NOTIFY, WM_DRAWITEM, WM_COMMAND, WM_CONTEXTMENU, и возможно
              другие в будущем, перехваченные у родительского окошка (до того как
              он о них узнает). В случае если SevMessageProc возвращает значение
              отличающееся от -1 то даже стандартная процедура окна windows не
              узнает об этом сообщении, а отправитель сообщения получит
              результат от SevMessageProc. К примеру объект CButton получит
              WM_COMMAND в свою SevMessageProc при клике по кнопке его виджета,
              что позволяет создавать "независимые" оконные классы, которые
              содержат ВСЮ логику обработки сообщений, без необходимости
              создавать "подстилку" под него для отлова сообщений этого класса.
    */
    virtual LRESULT SevMessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
    /**
    @brief    Функция переразмещения.
    @details  в реализации потомков должна:
              - разместить все дочерние окна, создав новый экземпляр WPOS на основе своего
              - разместить себя путём передачи управления CWindow (или вручную)
              порядок действий может меняться.
              пример:
              @code
                void MyWin::ReSize(WPOS &position)
                {
                    inherited::ReSize(position);
                    WPOS loc(&position, handle());
                    loc.padding = (int) em.cy * 1.2;
                    loc.margin = (int) em.cy * 1.5;
                    widget1.ReSize(loc.slice_top(widget1.em.cy + 2));
                    widget2.ReSize(loc.set_next_row());
                    widget3.ReSize(loc.set_next_row());
                }
              @endcode
    */
    virtual void ReSize(WPOS &position);

    /// Позволяет использовать объекты CWindow там где требуется HWND
    operator HWND() const {return windowHandle;};

    /// Позволяет получить дескриптор окошка там где неудобен operator HWND()
    HWND handle() const {return windowHandle;};

    /// Записывает в size размеры заглавной буквы M для шрифта
    static bool GetFontEM(HFONT hFont, SIZE& size);

private:
    volatile int modalResult; //!< изменяется через косвенный вызов!
    WNDPROC originalWindowProc; //!< родная функция окошка
    HWND windowHandle; //!< дескриптор окошка (не менять значение ни в коем разе!)

protected:
    HACCEL accelTable; //!< таблица акселераторов (уничтожается в ~CWindow())
    /// приводит к прерыванию цикла Run() если mr != MODAL_RESULT::NONE
    virtual bool SetModalResult(int ModalResult);
};

} // namespace gui
#endif // CWINDOW_H_INC
