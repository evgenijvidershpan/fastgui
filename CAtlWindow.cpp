/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "CAtlWindow.h"
#include "CLog.h"

namespace gui
{

class DLL_ATL: noncopyable
{
public:
    static DLL_ATL& Instance()
    {
        static DLL_ATL inst;
        return inst;
    };
    static BOOL AtlAxWinInit(void)
    {
        DLL_ATL& This = Instance();
        if (This.AtlAxWinInitProc)
            return This.AtlAxWinInitProc();
        return FALSE;
    };
    static HRESULT AtlAxGetHost(HWND h, IUnknown** pp)
    {
        DLL_ATL& This = Instance();
        if (This.AtlAxGetHostProc)
            return This.AtlAxGetHostProc(h, pp);
        return E_POINTER;
    };
    ~DLL_ATL()
    {
        AtlAxWinInitProc = 0;
        AtlAxGetHostProc = 0;
        if (hModule)
        {
            FreeLibrary(hModule);
            hModule = 0;
        }
    };
private:
    DLL_ATL()
    {
        AtlAxWinInitProc = 0;
        AtlAxGetHostProc = 0;
        hModule = LoadLibrary(TEXT("atl"));
        if (hModule)
        {
            *(FARPROC*) &AtlAxWinInitProc =
                GetProcAddress(hModule, "AtlAxWinInit");
            *(FARPROC*) &AtlAxGetHostProc =
                GetProcAddress(hModule, "AtlAxGetHost");
        }
        else
        {
            CLog::Instance().Msg(TEXT("atl.dll load fail."), CLog::Error);
        }
    };
    HMODULE hModule;
#ifdef _WIN64
    BOOL (*AtlAxWinInitProc)();
    HRESULT (*AtlAxGetHostProc)(HWND, IUnknown**);
#else
    BOOL (__stdcall *AtlAxWinInitProc)();
    HRESULT (__stdcall *AtlAxGetHostProc)(HWND, IUnknown**);
#endif
};


bool CAtlWindow::Create(CCP& cp)
{
    if (DLL_ATL::AtlAxWinInit())
        cp.lpClassName = TEXT("AtlAxWin");
    else
        cp.lpClassName = TEXT("ATLFAILBACK");
    return CCustomWC::Create(cp);
}


IID IID_HOST = {0xb6ea2050,0x48a,0x11d1,{0x82,0xb9,0x0,0xc0,0x4f,0xb9,0x94,0x2e}};
DECLARE_INTERFACE_(IHOST, IUnknown)
{
    STDMETHOD(NU1)(LPVOID,LPVOID,LPVOID) PURE;
    STDMETHOD(NU2)(LPVOID,LPVOID,LPVOID,LPVOID,LPVOID,LPVOID) PURE;
    STDMETHOD(NU3)(LPVOID,LPVOID) PURE;
    STDMETHOD(QCL)(REFIID riid, void **ppv) PURE;
    STDMETHOD(NU4)(LPVOID) PURE;
    STDMETHOD(NU5)(LPVOID) PURE;
};


HRESULT CAtlWindow::GetControl(const IID& riid, void** ppv)
{
    if (!ppv)
        return E_POINTER;
    if (!IsWindow(handle()))
        return E_HANDLE;

    if (!lpUnknown)
    {
        IUnknown* lphost = 0;
        if (FAILED(DLL_ATL::AtlAxGetHost(handle(), &lphost)))
            return E_POINTER;

        HRESULT hr = lphost->QueryInterface(IID_HOST, (void**)&lpUnknown);
        lphost->Release();
        lphost = 0;

        if (FAILED(hr))
        {
            lpUnknown = 0;
            return E_NOINTERFACE;
        }
    }
    if (!lpUnknown)
        return E_POINTER;

    IHOST* lpHost = (IHOST*) lpUnknown;
    return lpHost->QCL(riid, ppv);
}


LRESULT CAtlWindow::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if (!lpUnknown)
    {
        if (uMsg == WM_PAINT)
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(handle(), &ps);
            RECT rc;
            GetClientRect(handle(), &rc);
            SetTextColor(hdc, RGB(0xFF, 0, 0));
            SetBkMode(hdc, TRANSPARENT);
            TCHAR buf[256];
            buf[0] = 0;
            GetWindowText(handle(), buf, 256-32);
            lstrcat(buf, TEXT(" - error ATL host!"));
            DrawText(hdc, buf, -1, &rc, DT_NOPREFIX|\
               DT_VCENTER|DT_CENTER|DT_SINGLELINE);
            EndPaint(handle(), &ps);
        }
        else if (uMsg == WM_ERASEBKGND)
        {
            RECT rc;
            GetClientRect(handle(), &rc);
            InvalidateRect(handle(), &rc, FALSE);
            FillRect((HDC)wParam, &rc, GetSysColorBrush(COLOR_WINDOW));
            return 1;
        }
    }
    return CCustomWC::MessageProc(uMsg, wParam, lParam);
}


CAtlWindow::CAtlWindow()
{
    lpUnknown = 0;
}


CAtlWindow::~CAtlWindow()
{
    if (lpUnknown)
    {
        lpUnknown->Release();
        lpUnknown = 0;
    }
}


} // namespace gui

