/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SWC_H_INC
#define SWC_H_INC

#include <windows.h>
#include <commctrl.h>

#include "importdefs.h"
#include "CWindow.h"

/** Set of Standard Window Classes (SWC)

Classes are currently available:

gui::CHeaderCtrl  gui::CButton
gui::CAnimate     gui::CEdit
gui::CHotkey      gui::CToolBar
gui::CProgress    gui::CListBox
gui::CToolTips    gui::CTreeWnd
gui::CTrackBar    gui::CComboBox
gui::CUpDown      gui::CComboBoxEx
gui::CStatusBar   gui::CStatic
gui::CScrollBar   gui::CTabCtrl
gui::CReBar       gui::CRishEdit
gui::CListView    gui::CRishEdit20

*/

namespace gui
{

/**
@brief    Базовый класс для "кастомных" оконных классов созданных пользователем
@details  Используется как основа для пользовательских классов, позволяет
          пропустить этапы регистрации классов в системе, позволяет пропустить
          написание WNDPROC для каждого класса.
*/
class GUI_IMPORT CCustomWC: public CWindow
{
public:
    virtual bool Create(CCP& cp);
};


/**
@brief    Базовый класс для "невидимых окошек"
@details  Используется как основа для пользовательских классов, задача которых
          лишь обработка сообщений (например для CTray**), также является
          средством обеспечения функционирования SevMessageProc() для элементов
          управления не имеющих родительского класса (назначается SharedHWND())
@sa       SevMessageProc()
*/
class GUI_IMPORT CMsgWindow :public CCustomWC
{
public:
    virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
    virtual bool Create(CCP& cp);
/**
@brief    Общее для всех окошко сообщений
@details  Служит вспомогательным средством, например можно назначать его как
          родительское тем классам которым нужно перехватить в SevMessageProc
          свои сообщения, но при этом они хотят быть окнами верхнего уровня
          Внимание! такие окошки должны посылать сообщения без условия WS_CHILD
*/
    static HWND SharedHWND();
};


/**
@brief    Базовый класс для "главных" окошек
@details  Используется как основа для пользовательских классов, задача которых
          отображение диалогового окошка ввода, отличие от CCustomWC -
          отрисовка в правом нижнем углу окошка - маркера изменения размера и
          автоматический вызов Resize() по WM_WINDOWPOSCHANGED
*/
class GUI_IMPORT CCustomDlgWC :public CCustomWC
{
public:
    virtual bool Create(CCP& cp);
    virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
};


/***************************  стандартные классы  *****************************/

#ifndef WC_BUTTON
#define WC_BUTTON (TEXT("Button"))
#endif

/**
@brief    Стандартная кнопка "BUTTON"
*/
class GUI_IMPORT CButton :public CWindow
{
public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_BUTTON;
        return CWindow::Create(cp);
    };
};


#ifndef WC_EDIT
#define WC_EDIT (TEXT("Edit"))
#endif

/**
@brief   Стандартное поле ввода "Edit"
*/
class GUI_IMPORT CEdit :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_EDIT;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартная панель кнопок "ToolbarWindow32"
*/
class GUI_IMPORT CToolBar :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = TOOLBARCLASSNAME;
        return CWindow::Create(cp);
    };
    virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
};


#ifndef WC_LISTBOX
#define WC_LISTBOX (TEXT("ListBox"))
#endif

/**
@brief   Стандартный список строк "ListBox"
*/
class GUI_IMPORT CListBox :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_LISTBOX;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный древовидный список "SysTreeView32"
*/
class GUI_IMPORT CTreeWnd :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_TREEVIEW;
        return CWindow::Create(cp);
    };
};


#ifndef WC_COMBOBOX
#define WC_COMBOBOX (TEXT("ComboBox"))
#endif

/**
@brief   Стандартный выпадающий список "ComboBox"
*/
class GUI_IMPORT CComboBox :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_COMBOBOX;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный выпадающий список "ComboBox" с перегруженной функцией поля ввода
*/
class GUI_IMPORT CComboBoxEx :public CWindow
{
    static LRESULT CALLBACK combobox_edit_procedure(HWND,UINT,WPARAM,LPARAM);
    WNDPROC editOriginalProc; //!< оригинальная процедура окошка ввода
    protected:
        HWND editHandle; //!< окошко ввода на ComboBox
    public:
        CComboBoxEx();
        ~CComboBoxEx();
        virtual bool Create(CCP& cp);
        /// оконная процедура поля ввода у ComboBox
        virtual LRESULT EditMessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
};


#ifndef WC_STATIC
#define WC_STATIC (TEXT("Static"))
#endif

/**
@brief   Стандартный класс windows "STATIC"
*/
class GUI_IMPORT CStatic :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_STATIC;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс вкладок "SysTabControl32"
*/
class GUI_IMPORT CTabCtrl :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_TABCONTROL;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс заголовков "SysHeader"
*/
class GUI_IMPORT CHeaderCtrl :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_HEADER;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс поддержки анимаций "SysAnimate32"
*/
class GUI_IMPORT CAnimate :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = ANIMATE_CLASS;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс поля ввода сочетания клавиш "msctls_hotkey32"
*/
class GUI_IMPORT CHotkey :public CWindow
{
    public:
    virtual bool Create(CCP& cp);
};


/**
@brief   Стандартный класс отображения прогресса "msctls_progress"
*/
class GUI_IMPORT CProgress :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = PROGRESS_CLASS;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс отображения всплывающих подсказок "tooltips_class"
*/
class GUI_IMPORT CToolTips :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = TOOLTIPS_CLASS;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс позиционного переключателя "msctls_trackbar32"
*/
class GUI_IMPORT CTrackBar :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = TRACKBAR_CLASS;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс "msctls_updown32"
*/
class GUI_IMPORT CUpDown :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = UPDOWN_CLASS;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс статусной строки "msctls_statusbar"
*/
class GUI_IMPORT CStatusBar :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = STATUSCLASSNAME;
        return CWindow::Create(cp);
    };
    virtual void ReSize(WPOS &position);
};


#ifndef WC_SCROLLBAR
#define WC_SCROLLBAR (TEXT("ScrollBar"))
#endif

/**
@brief   Стандартный класс полоса прокрутки "ScrollBar"
*/
class GUI_IMPORT CScrollBar :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_SCROLLBAR;
        return CWindow::Create(cp);
    };
};


#ifndef REBARCLASSNAME
#define REBARCLASSNAME (TEXT("ReBarWindow32"))
#endif

/**
@brief   Стандартный класс управлениея набором панелей "ReBarWindow32"
*/
class GUI_IMPORT CReBar :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = REBARCLASSNAME;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс управлениея набором панелей "ReBarWindow32"
*/
class GUI_IMPORT CListView :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = WC_LISTVIEW;
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс базовой поддержки форматированого текста "RICHEDIT"
*/
class GUI_IMPORT CRishEdit :public CWindow
{
    public:
    virtual bool Create(CCP& cp){
        cp.lpClassName = TEXT("RICHEDIT");
        return CWindow::Create(cp);
    };
};


/**
@brief   Стандартный класс расширеной поддержки форматированого текста "RichEdit20W"
*/
class GUI_IMPORT CRishEdit20 :public CWindow
{
    public:
    #if UNICODE
    virtual bool Create(CCP& cp){
        cp.lpClassName = L"RichEdit20W";
        return CWindow::Create(cp);
    };
    #else
    virtual bool Create(CCP& cp){
        cp.lpClassName = "RichEdit20A";
        return CWindow::Create(cp);
    };
    #endif
};


} // namespace gui

#endif
