#include "fastgui.h"

using namespace gui;

class MyButton: public CButton
{
    virtual LRESULT SevMessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
    {
        if (uMsg == WM_COMMAND)
        {
            SetWindowText(handle(), TEXT("clicked!"));
            MessageBox(GetParent(handle()), TEXT("clicked!"), 0, 0);
            return 0;
        }
        return CButton::SevMessageProc(uMsg, wParam, lParam);
    };
};

class MyDialog: public CCustomDlgWC
{
    MyButton m_ok;
public:
    virtual bool Create(CCP& cp)
    {
        if (CCustomDlgWC::Create(cp))
        {
            CCP ccp(handle());
            ccp.set(WS_VISIBLE|WS_CHILD|BS_PUSHBUTTON,
                    0, TEXT("Hello!"),
                    10, 10, 120, 50);
            return m_ok.Create(ccp);
        }
        return false;
    };
};


int main()
{
    MyDialog dialog;
    CCP ccp(WS_OVERLAPPEDWINDOW, 0, TEXT("hello world!"), 150, 100);
    if (dialog.Create(ccp))
    {
        return dialog.Run(0);
    }
    return -1;
}
