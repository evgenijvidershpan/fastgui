void onDraw(LPDRAWITEMSTRUCT lpdis)
{
    RECT& rci = lpdis->rcItem;
    RECT rcl = {0, 0, rci.right-rci.left, rci.bottom-rci.top};
    CBitmap bmp(CreateCompatibleBitmap(lpdis->hDC, rcl.right, rcl.bottom));
    if (!bmp.handle())
        return; // без фейлбэк (нет ресурсов у системы)
    CDCMemory mdc(lpdis->hDC);
    if (!mdc.handle())
        return; // без фейлбэк (нет ресурсов у системы)
    HGDIOBJ old = SelectObject(mdc, bmp);
    // рисуем на mdc что нам нужно используя локальные координаты

    FillRect(mdc, &rcl, CBrush(CreateSolidBrush(
        (lpdis->itemState & ODS_SELECTED)?
            RGB(0,0,255) : RGB(255,255,255))));

    // переносим нарисованное на lpdis->hDC используя оригинальные координаты
    BitBlt(lpdis->hDC, rci.left, rci.top, rcl.right, rcl.bottom,
        mdc, 0, 0, SRCCOPY);
    SelectObject(mdc, old);
    // обращаемся к bmp чтобы оптимизатор точно не удалил bmp до этого момента
    bmp.free();
    return;
}
