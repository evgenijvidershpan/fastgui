#include "fastgui.h"

using namespace gui;

int main()
{
    CButton window;
    CCP ccp(WS_OVERLAPPEDWINDOW, 0, TEXT("hello world!"), 150, 100);
    window.Create(ccp);
    return window.Run(0);
}
