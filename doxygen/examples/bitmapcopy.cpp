HBITMAP bitmapCopy(CWindow& target)
{
    CDCWindow dc(target, true);
    if (!dc.handle())
        return 0;
    CDCMemory mdc(dc);
    if (!mdc.handle())
        return 0;
    CBitmap bmp(CreateCompatibleBitmap(dc, 100, 100));
    if (!bmp.handle())
        return 0;
    HGDIOBJ old = SelectObject(mdc, bmp);
    BitBlt(mdc, 0, 0, 100, 100, dc, 0, 0, SRCCOPY);
    SelectObject(mdc, old);
    return (HBITMAP) bmp.detach();
}
