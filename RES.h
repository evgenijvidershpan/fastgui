/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FASTGUI_RES_H_INC
#define FASTGUI_RES_H_INC

#include <windows.h>
#include "importdefs.h"
#include "CLog.h"

namespace gui
{

/**
@brief    Автоматический IMAGELIST
*/
class GUI_IMPORT CImageList: noncopyable
{
    HIMAGELIST hImageList;
public:
    /// Позволяет использовать объекты CImageList там где требуется HIMAGELIST
    operator HIMAGELIST() const {return hImageList;};
    /// Позволяет получить HIMAGELIST там где неудобен operator HIMAGELIST()
    HIMAGELIST handle() const {return hImageList;};
    /// Инициализация с захватом Image List handle
    explicit CImageList(HIMAGELIST handle = NULL)
    {
        hImageList = handle;
    };
    /// "Присоединение" к HIMAGELIST (CImageList освобождает дескриптор сам)
    HIMAGELIST attach(HIMAGELIST handle)
    {
        if (!free())
            LOG_ERROR(TEXT("ImageList_Destroy fail."));
        hImageList = handle;
        return hImageList;
    };
    /// "Отсоединение" от HIMAGELIST (дальше ваша забота об уничтожении дескриптора)
    HIMAGELIST detach()
    {
        HIMAGELIST rVal = hImageList;
        hImageList = NULL;
        return rVal;
    };
    /// "Освобождение" HIMAGELIST
    BOOL free()
    {
        if (!hImageList)
            return TRUE;
        if (!ImageList_Destroy(hImageList))
            return FALSE;
        hImageList = NULL;
        return TRUE;
    };
    /// "освобождает" захваченный HIMAGELIST если есть.
    ~CImageList()
    {
        if (!free())
            LOG_ERROR(TEXT("ImageList_Destroy fail."));
    };
};



/**
@brief    Автоматический HGDIOBJ
*/
class GUI_IMPORT CGdiObject: noncopyable
{
public:
    /// создание с "захватом" hObj
    explicit CGdiObject(HGDIOBJ hObj):
        hObject(hObj)
    {
    };
    ~CGdiObject()
    {
        if (!free())
            LOG_ERROR(TEXT("DeleteObject fail."));
        hObject = NULL;
    };
    /// "Освобождение" HGDIOBJ
    BOOL free()
    {
        if (hObject)
        {
            if (!DeleteObject(hObject))
                return FALSE;
            hObject = NULL;
        }
        return TRUE;
    };
    /// "Отсоединение" от HGDIOBJ (дальше ваша забота об уничтожении дескриптора)
    HGDIOBJ detach()
    {
        HGDIOBJ rVal = hObject;
        hObject = NULL;
        return rVal;
    };
    /// Захват во владение HGDIOBJ
    HGDIOBJ attach(HGDIOBJ handle)
    {
        if (!free())
            LOG_ERROR(TEXT("DeleteObject() fail."));
        hObject = handle;
        return handle;
    };
protected:
    /// собственно сам дескриптор.
    HGDIOBJ hObject;
};


/**
@brief    Автоматический HBITMAP

          Пример использования
          @include bitmapcopy.cpp
*/
class GUI_IMPORT CBitmap: public CGdiObject
{
public:
    /// Позволяет использовать объекты CBitmap там где требуется HBITMAP
    operator HBITMAP() const {return (HBITMAP) hObject;};
    /// Позволяет получить HBITMAP там где неудобен operator HBITMAP()
    HBITMAP handle() const {return (HBITMAP) hObject;};
    /// Инициализация
    explicit CBitmap(HBITMAP hBmp=NULL):
        CGdiObject((HGDIOBJ) hBmp)
    {
    };
};

/**
@brief    Автоматический HBRUSH

          Пример ниже создаёт цветную кисть, рисует и освобождает её
          автоматически.
          @include brushuse.cpp
*/
class GUI_IMPORT CBrush: public CGdiObject
{
public:
    /// Позволяет использовать объекты CBrush там где требуется HBRUSH
    operator HBRUSH() const {return (HBRUSH) hObject;};
    /// Позволяет получить HBRUSH там где неудобен operator HBRUSH()
    HBRUSH handle() const {return (HBRUSH) hObject;};
    /// Инициализация
    explicit CBrush(HBRUSH hBrush=NULL):
        CGdiObject((HGDIOBJ) hBrush)
    {
    };
};

/**
@brief    Автоматический HPEN
*/
class GUI_IMPORT CPen: public CGdiObject
{
public:
    /// Позволяет использовать объекты CPen там где требуется HPEN
    operator HPEN() const {return (HPEN) hObject;};
    /// Позволяет получить HPEN там где неудобен operator HPEN()
    HPEN handle() const {return (HPEN) hObject;};
    /// Инициализация
    explicit CPen(HPEN hPen=NULL):
        CGdiObject((HGDIOBJ) hPen)
    {
    };
};


/**
@brief    Автоматический HFONT
*/
class GUI_IMPORT CFont: public CGdiObject
{
public:
    /// Позволяет использовать объекты CFont там где требуется HFONT
    operator HFONT() const {return (HFONT) hObject;};
    /// Позволяет получить HFONT там где неудобен operator HFONT()
    HFONT handle() const {return (HFONT) hObject;};
    /// Инициализация
    explicit CFont(HFONT hFont=NULL):
        CGdiObject((HGDIOBJ) hFont)
    {
    };
};

/**
@brief    Автоматический HRGN
*/
class GUI_IMPORT CRgn: public CGdiObject
{
public:
    /// Позволяет использовать объекты CRgn там где требуется HRGN
    operator HRGN() const {return (HRGN) hObject;};
    /// Позволяет получить HRGN там где неудобен operator HRGN()
    HRGN handle() const {return (HRGN) hObject;};
    /// Инициализация
    explicit CRgn(HRGN hrgn=NULL):
        CGdiObject((HGDIOBJ) hrgn)
    {
    };
};

namespace scoped
{
/**
@brief    Автоматический селектор для DC
*/
class GUI_IMPORT selobj: gui::noncopyable
{
protected:
    HDC hdc;
    HGDIOBJ org;
public:
    explicit selobj(HDC dc, HGDIOBJ obj):
        hdc(dc), org(0)
    {
        org = SelectObject(dc, obj);
    };

    ~selobj()
    {
        if (hdc && org && org != (HGDIOBJ)(INT_PTR) GDI_ERROR)
            SelectObject(hdc, org);
        org = 0;
        hdc = 0;
    };
};

/**
@brief    Автоматический селектор для DC с удалением объекта
*/
class GUI_IMPORT seltmp: selobj
{
    HGDIOBJ obj;
public:
    explicit seltmp(HDC dc, HGDIOBJ tmp): selobj(dc, tmp), obj(tmp)
    {
    };

    ~seltmp()
    {
        if (hdc && org && org != (HGDIOBJ)(INT_PTR) GDI_ERROR)
            SelectObject(hdc, org);
        org = 0;
        hdc = 0;
        DeleteObject(obj);
        obj = 0;
    };
};

/**
@brief    Автоматический (device context)
*/
class GUI_IMPORT cdc: noncopyable
{
    HDC hdc;
    HGDIOBJ org;
    HWND win;
    HGDIOBJ hAuto;
public:
    /// Позволяет использовать объекты gui::scoped::cdc там где требуется HDC
    operator HDC() const {return hdc;};
    /// Позволяет получить HDC там где неудобен operator HDC()
    HDC handle() const {return hdc;};
    /// Инициализация с захватом dc
    explicit cdc(HDC dc):
        hdc(dc), org(0), win(0), hAuto(0)
    {
    };

    explicit cdc(HWND hWin, bool bClient):
        hdc(0), org(0), win(hWin), hAuto(0)
    {
        if (!hWin)
            return;
        hdc = bClient? GetDC(hWin) : GetWindowDC(hWin);
    };

    ~cdc()
    {
        Select(NULL);
        if (!hdc)
            return;
        if (win)
            ReleaseDC(win, hdc);
        else
            DeleteDC(hdc);
        org = 0;
        win = 0;
        hdc = 0;
    };

    BOOL Select(HGDIOBJ gobj, BOOL AutoDelete = 0)
    {
        if (hAuto)
        {
            DeleteObject(hAuto);
            hAuto = 0;
        }
        if (AutoDelete)
            hAuto = gobj;
        if (!hdc)
            return FALSE;

        if (org != (HGDIOBJ)(INT_PTR) GDI_ERROR && org)
            SelectObject(hdc, org);
        org = 0;
        if (!gobj)
            return FALSE;
        org = SelectObject(hdc, gobj);
        return org != (HGDIOBJ)(INT_PTR) GDI_ERROR && org;
    };
};

} // namespace scoped

/**
@brief    Автоматический (memory device context)
*/
class GUI_IMPORT CDCMemory
{
    HDC dcm;
public:
    /// Позволяет использовать объекты CDCMemory там где требуется HDC
    operator HDC() const {return dcm;};
    /// Позволяет получить HDC там где неудобен operator HDC()
    HDC handle() const {return dcm;};
    /// Создаёт совместимый с dc контекст в памяти
    explicit CDCMemory(HDC dc)
    {
        dcm = CreateCompatibleDC(dc);
    };
    ~CDCMemory()
    {
        if (dcm)
        {
            DeleteDC(dcm);
            dcm = NULL;
        }
    };
};


/**
@brief    Автоматический (window device context)
*/
class GUI_IMPORT CDCWindow
{
    HDC dcm;
    HWND hWindow;

public:
    /// Позволяет использовать объекты CDCMemory там где требуется HDC
    operator HDC() const {return dcm;};
    /// Позволяет получить HDC там где неудобен operator HDC()
    HDC handle() const {return dcm;};
    /// Инициализация
    CDCWindow(HWND hWin, bool bClient)
    {
        hWindow = hWin;
        dcm = bClient? GetDC(hWin) : GetWindowDC(hWin);
    };
    ~CDCWindow()
    {
        if (dcm)
        {
            ReleaseDC(hWindow, dcm);
            dcm = NULL;
        }
    };
};


} // namespace gui

#endif // FASTGUI_RES_H_INC
