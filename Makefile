# This file is a part of fastgui library.
# Copyright © 2007-2019 Видершпан Евгений Сергеевич
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# наш проект собирается PSDK для версии не ниже 5.0 (win98)
!IFNDEF  APPVER
!MESSAGE make: Set APPVER to 5.0 (default)
APPVER   = 5.0
OPTS     = APPVER="5.0"
!ENDIF

!include <win32.mak>

!IF $(NMAKE_WINVER) == 0x0400
!ERROR use the platform version 5.0 or higher (use: nmake APPVER="5.0")
!ENDIF

!IFNDEF  OUTDIR
!IFNDEF  NODEBUG
OUTDIR   = BUILD\PSDK$(APPVER)\Debug
!ELSE
OUTDIR   = BUILD\PSDK$(APPVER)\Release
!ENDIF
!ENDIF

!IFNDEF  RX_FLAGS
RX_FLAGS = -DUNICODE -D_UNICODE
!ENDIF

PROJ     = fastgui

PROJ_OBJ = \
    $(OUTDIR)\CWindow.obj \
    $(OUTDIR)\CLog.obj \
    $(OUTDIR)\SWC.obj \
    $(OUTDIR)\CTrayIcon.obj \
    $(OUTDIR)\CAtlWindow.obj

all:    $(OUTDIR) $(OUTDIR)\$(PROJ).lib

$(OUTDIR):
     if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# Generic rule for building ALL CPP files and placing their OBJ's in the OUTDIR
.cpp{$(OUTDIR)}.obj:
    $(cc) $(cdebug) $(cflags) $(cvarsmt) \
    $(RX_FLAGS) /Fo"$(OUTDIR)\\" /Fd"$(OUTDIR)\\" $**

# Build rule for LIB
$(OUTDIR)\$(PROJ).lib: $(PROJ_OBJ)
    $(link) /LIB $(ldebug) /OUT:$@ /MACHINE:$(CPU) $**

clean:
    -if exist $(OUTDIR)\$(PROJ).lib del $(OUTDIR)\$(PROJ).lib
    -if exist $(OUTDIR)\CWindow.obj del $(OUTDIR)\CWindow.obj
    -if exist $(OUTDIR)\CLog.obj del $(OUTDIR)\CLog.obj
    -if exist $(OUTDIR)\SWC.obj del $(OUTDIR)\SWC.obj
    -if exist $(OUTDIR)\CTrayIcon.obj del $(OUTDIR)\CTrayIcon.obj
    -if exist $(OUTDIR)\CAtlWindow.obj del $(OUTDIR)\CAtlWindow.obj
