/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "CLog.h"

namespace gui
{

CLog::CLog()
{
    hTarget = 0;
}

CLog& CLog::Instance()
{
    static CLog log;
    return log;
}

void CLog::SetWindow(HWND hWindow)
{
    hTarget = hWindow;
}


void CLog::Msg(LPCTSTR str, int type)
{
    if (!str)
        return;
    if (hTarget)
    {
        SendMessage(hTarget, WM_LOG_EVENT, type, (LPARAM)str);
    }
    else
    {
        HANDLE hOut = GetStdHandle(STD_ERROR_HANDLE);
        if (hOut == INVALID_HANDLE_VALUE)
            return;
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        BOOL bSaved = GetConsoleScreenBufferInfo(hOut, &csbi);

        const TCHAR* szType = 0;
        switch (type)
        {
            case CLog::Warning:
            szType = TEXT("warning");
            if (bSaved)
                SetConsoleTextAttribute(hOut,
                    FOREGROUND_RED|FOREGROUND_GREEN);
            break;
            case CLog::Error:
            if (bSaved)
                SetConsoleTextAttribute(hOut,
                    FOREGROUND_RED|FOREGROUND_INTENSITY);
            szType = TEXT("error");
            break;
            case CLog::Log:
            szType = TEXT("log");
            if (bSaved)
                SetConsoleTextAttribute(hOut,
                    FOREGROUND_RED|FOREGROUND_BLUE|FOREGROUND_GREEN|\
                    FOREGROUND_INTENSITY);
            break;
        }
        DWORD bw = 0;
        TCHAR buf[64];
        if (szType)
            wsprintf(buf, TEXT("%s: "), szType);
        else
        {
            wsprintf(buf, TEXT("msg(%d): "), type);
            if (bSaved)
                SetConsoleTextAttribute(hOut,
                    FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_INTENSITY);
        }
        WriteConsole(hOut, buf, lstrlen(buf), &bw, 0);
        int len = lstrlen(str);
        WriteConsole(hOut, str, lstrlen(str), &bw, 0);
        if (len > 0)
        {
            if (str[len-1] != '\n')
                WriteConsole(hOut, TEXT("\n"), 1, &bw, 0);
        }
        if (bSaved)
            SetConsoleTextAttribute(hOut, csbi.wAttributes);
    }
}


void CLog::SysMsg(LPCTSTR str, int error, int type)
{
    TCHAR* buff = 0;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        0, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &buff, 0, 0);
    if (!buff)
    {
        Msg(TEXT("can't get description from OS."), CLog::Error);
        return;
    }
    if (!str)
    {
        Msg(buff, type);
        LocalFree(buff);
        return;
    }
    int bufflen = lstrlen(buff);
    int msglen = lstrlen(str);

    TCHAR* result = (TCHAR*)LocalAlloc(LMEM_FIXED,
        (msglen + bufflen + 2) * sizeof(TCHAR));
    if (result)
    {
        lstrcpy(result, str);
        result[msglen] = '\n';
        lstrcpy(result+msglen+1, buff);
        Msg(result, type);
        LocalFree(result);
    }
    LocalFree(buff);
}

} // namespace gui
