/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "CWindow.h"
#include "CLog.h"

namespace gui
{


WPOS::WPOS(WPOS* wParent, HWND hParent)
{
    m_parent = wParent;

    if (wParent)
    {
        margin = wParent->margin;
        padding = wParent->padding;
    }
    else
    {
        padding = 0;
        margin = 0;
    }

    if (hParent)
    {
        RECT rc;
        if (GetClientRect(hParent, &rc))
        {
            left = padding;
            top = padding;
            right = rc.right - padding;
            bottom = rc.bottom - padding;
            return;
        }
    }
    left = top = right = bottom = 0;
}



CWindow::CWindow()
{
    frameHandle = 0;
    windowHandle = 0;
    accelTable = 0;
    modalResult = MODAL_RESULT::NONE;
    originalWindowProc = 0;
    // пытаемся определить размеры шрифта для окошка
    em.cx = 12;
    em.cy = 16;
    HFONT hfont = (HFONT) GetStockObject(DEFAULT_GUI_FONT);
    if (!hfont)
        hfont = (HFONT) GetStockObject(SYSTEM_FONT);
    if (!hfont)
    {
        LOG_ERROR(TEXT("can't get the default font"));
        return;
    }
    GetFontEM(hfont, em);
}


CWindow::~CWindow()
{
    if ( originalWindowProc )
    {
        // Восстановление старого обработчика сообщений
        #ifdef GWLP_WNDPROC
        SetWindowLongPtr(windowHandle, GWLP_WNDPROC, (LONG_PTR)originalWindowProc);
        #else
        SetWindowLong(windowHandle, GWL_WNDPROC, (DWORD)originalWindowProc);
        #endif
    }
    if (IsWindow(windowHandle))
    {
        if (frameHandle == windowHandle)
        {
            frameHandle = 0;
        }
        if (!DestroyWindow(windowHandle))
        {
            LOG_SYSERROR(TEXT("can't destroy window."));
        }
        windowHandle = 0;
    }
    if (accelTable)
    {
        DestroyAcceleratorTable(accelTable);
    }
}


bool CWindow::GetFontEM(HFONT hFont, SIZE& size)
{
    if (!hFont)
        return false;

    CLog& log = CLog::Instance();

    HDC hdc = CreateDC(TEXT("DISPLAY"), 0, 0, 0);
    if (!hdc)
    {
        log.Msg(TEXT("can't get DC."), CLog::Error);
        return false;
    }
    HDC hmdc = CreateCompatibleDC(hdc);
    if (!hmdc)
    {
        log.Msg(TEXT("can't get mem DC."), CLog::Error);
        DeleteDC(hdc);
        return false;
    }
    bool rVal = false;
    HGDIOBJ old = SelectObject(hmdc, hFont);
    GetTextExtentPoint32(hmdc, TEXT("M"), 1, &size);
    /*
    TEXTMETRIC tm;
    if (GetTextMetrics(hmdc, &tm))
    {
        size.cy = tm.tmHeight;
        size.cx = tm.tmAveCharWidth;
        // ssize.cx = tm.tmAveCharWidth + tm.tmOverhang;
        rVal = true;
    }*/
    SelectObject(hmdc, old);
    DeleteDC(hmdc);
    DeleteDC(hdc);
    return rVal;
}


bool CWindow::BeforeEnterMsgLoop(HWND hWndOwn)
{
    ShowWindow(windowHandle, SW_SHOW);
    UpdateWindow(windowHandle);

    if (GetWindowLong(windowHandle, GWL_STYLE) & WS_CHILD)
    {
        LOG_ERROR(TEXT("child window can't be modal."));
        return false;
    }

    if (IsWindow(hWndOwn))
    {
        HWND hRoot = GetAncestor(hWndOwn, GA_ROOT);
        if (IsWindow(hRoot))
        {
            SetProp(windowHandle, TEXT("CWRBLK"), hRoot);
            EnableWindow(hRoot, false);
        }
    }
    SetForegroundWindow(windowHandle);
    SetActiveWindow(windowHandle);
    return true;
}


bool CWindow::SetModalResult(int mr)
{
    modalResult = mr;
    PostMessage(windowHandle, WM_CANCELMODE, 0, 0);
    return true;
}


int CWindow::Run(HWND hModalParent)
{
    modalResult = MODAL_RESULT::NONE;

    if (BeforeEnterMsgLoop(hModalParent) == false)
        return MODAL_RESULT::NONE;

    MSG msg;
    while ( IsWindow(windowHandle) &&
            GetMessage(&msg, NULL, 0, 0 ) &&
            modalResult == MODAL_RESULT::NONE )
    {
        bool bTranslate = true;
        if ( msg.message == WM_KEYDOWN ||
             msg.message == WM_SYSKEYDOWN ||
             msg.message == WM_CHAR ||
             msg.message == WM_SYSCHAR
           )
        {
            // начинаем искать цели трансляции с адресата сообщения
            HWND hTarget = msg.hwnd;
            HWND hframe = NULL;
            // пытаемся "перекрываемо" транслировать сообщение
            // - по очереди перебираем окна от цели к родительскому
            // в поисках цели трансляции.
            // - ищем первое FRAME (которое является FAILBACK - обработчиком)
            // в случае если ни одно окошко не обработало сочетание
            // - пытаемся транслировать сообщение для FRAME
            for(;;)
            {
                DWORD dwstyle = GetWindowLong(hTarget, GWL_STYLE);
                // если окошко отключено, дальше нет смысла искать
                if (dwstyle & WS_DISABLED)
                    break;
                // нужно проверить принадлежность окошка к текущему потоку
                if (GetWindowThreadProcessId(hTarget, 0) != GetCurrentThreadId())
                    break;
                CWindow* cwTarget = GETCWINDOW(hTarget);
                if (cwTarget)
                {
                    // первое фреймовое окно на пути становится основным
                    if (!hframe)
                        hframe = cwTarget->frameHandle;
                    // если акселератор для окошка установлен
                    if (cwTarget->accelTable &&
                        TranslateAccelerator(hTarget,
                                             cwTarget->accelTable,
                                             &msg))
                    {
                        // покинуть цикл если найден подходящий обработчик
                        bTranslate = false;
                        break;
                    }
                }
                // покинуть цикл если достигнуто FRAME окно
                if (hTarget == hframe)
                    break;
                // если нет родительского окна
                if (!(dwstyle & WS_CHILD))
                {
                    // значит пора попробовать для FRAME
                    if (hframe)
                    {
                        hTarget = hframe;
                        continue;
                    }
                    // нет FRAME значит больше нечего искать
                    break;
                }
                // повторить попытку трансляции для родительского окна
                hTarget = GetParent(hTarget);
                if (hTarget)
                    continue;
                break;
            }
        }
        // если сообщение не было обработано
        if (bTranslate)
        {
            HWND hparent = GetAncestor(msg.hwnd, GA_PARENT);
            if (hparent && IsDialogMessage(hparent, &msg))
            {
                bTranslate = false;
            }
        }

        if ( bTranslate )
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        // к этому моменту modalResult может измениться, но компилятору об этом
        // неизвестно! перечитывать modalResult обязательно.
        // класс вызвавший Run, может установить modalResult в значение не равное
        // MODAL_RESULT::NONE для: если
        if ( modalResult != MODAL_RESULT::NONE )
        {
            break;
        }
    }
    AfterEnterMsgLoop(hModalParent);
    if (modalResult == MODAL_RESULT::NONE)
        modalResult = MODAL_RESULT::CANCEL;
    return modalResult;
}



bool CWindow::AfterEnterMsgLoop(HWND hWndOwn)
{
    HWND TrueParent = (HWND)GetProp(windowHandle, TEXT("CWRBLK"));
    if (IsWindow(TrueParent))
    {
        EnableWindow(TrueParent, TRUE);
        SetForegroundWindow(TrueParent);
        SetActiveWindow(TrueParent);
        RemoveProp(windowHandle, TEXT("CWRBLK"));
    }
    else if (IsWindow(hWndOwn))
    {
        EnableWindow(hWndOwn, true);
        SetForegroundWindow(hWndOwn);
        SetActiveWindow(hWndOwn);
    }
    ShowWindow(windowHandle, SW_HIDE);
    return true;
}


void CWindow::ReSize(WPOS &pos)
{
    pos.apply_to(windowHandle);
}


static LRESULT CALLBACK shares_class_window_procedure(
    HWND hWnd,
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam )
{
    CWindow *pCw = GETCWINDOW(hWnd);
    if (pCw == 0)
    {
        return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }

    switch (uMsg)
    {
        case WM_NOTIFY:
        {
            LPNMHDR pnmh = (LPNMHDR) lParam;
            if (!pnmh)
                break;
            CWindow *cw = GETCWINDOW(pnmh->hwndFrom);
            if (cw)
            {
                LRESULT rVal = cw->SevMessageProc(uMsg, wParam, lParam);
                if (rVal != -1)
                {
                    return rVal;
                }
            }
        } break;

        case WM_DRAWITEM:
        {
            LPDRAWITEMSTRUCT lpdis = (LPDRAWITEMSTRUCT) lParam;
            if ( !lpdis )
                break;
            if (lpdis->CtlType != ODT_MENU)
            {
                CWindow *cw = GETCWINDOW(lpdis->hwndItem);
                if (cw)
                {
                    LRESULT rVal = cw->SevMessageProc(uMsg, wParam, lParam);
                    if (rVal != -1)
                    {
                        return rVal;
                    }
                }
            }
            else
            {
                /*
                CMenu* m = GetCMenu();
                if (m) {
                    return m->OnDraw(lpdis);
                }
                */
            }
        } break;

        case WM_COMMAND:
        {
            CWindow *cw = GETCWINDOW((HWND)lParam);
            if (cw)
            {
                LRESULT rVal = cw->SevMessageProc(uMsg, wParam, lParam);
                if (rVal != -1)
                {
                    return rVal;
                }
            }
        } break;

        case WM_CONTEXTMENU:
        {
            CWindow *cw = GETCWINDOW((HWND)wParam);
            if (cw)
            {
                LRESULT rVal = cw->SevMessageProc(uMsg, wParam, lParam);
                if (rVal != -1)
                {
                    return rVal;
                }
            }
        } break;
    }
    return pCw->MessageProc(uMsg, wParam, lParam);
}


LRESULT CWindow::SevMessageProc(UINT, WPARAM, LPARAM)
{
    return -1;
}


LRESULT CWindow::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if (!originalWindowProc)
    {
        LOG_ERROR(TEXT("recieve msg for not created window."));
        return 0;
    }
    switch (uMsg)
    {
        case WM_SETFONT:
        {
            GetFontEM((HFONT)wParam, em);
        } break;

        case WM_CLOSE:
        {
            SetModalResult(MODAL_RESULT::CANCEL);
        } return 0; // мы сами уничтожаем окошки когда нужно...
    }
    return CallWindowProc(originalWindowProc, windowHandle, uMsg, wParam, lParam);
}



// собственно создание окна
bool CWindow::Create(CCP& cp)
{
    CLog& log = CLog::Instance();
    if (!cp.lpClassName)
    {
        log.Msg(TEXT("cp.lpClassName is null!"), CLog::Error);
        return false;
    }

    frameHandle = NULL;
    if (cp.hWndParent && cp.hWndParent != HWND_MESSAGE)
    {
        CWindow *ppnt = GETCWINDOW(cp.hWndParent);
        if (ppnt)
        {
            frameHandle = ppnt->frameHandle;
        }
    }

    windowHandle = CreateWindowEx(cp.dwExStyle,
            cp.lpClassName,
            cp.lpWindowName,
            cp.dwStyle,
            cp.position.left,
            cp.position.top,
            cp.position.right,
            cp.position.bottom,
            cp.hWndParent,
            cp.hMenu,
            GetModuleHandle(NULL),
            0);

    if (!frameHandle)
    {
        frameHandle = windowHandle;
    }

    bool bRet = IsWindow(windowHandle) != FALSE;
    if ( bRet == false )
    {
        windowHandle = NULL;
        log.Msg(TEXT("CreateWindowEx failed."), CLog::Error);
        return false;
    }
    if ( SETCWINDOW(windowHandle, this ) == false )
    {
        DestroyWindow(windowHandle);
        log.Msg(TEXT("SETCWINDOW failed."), CLog::Error);
        return false;
    }

    #ifdef GWLP_WNDPROC
    originalWindowProc = (WNDPROC)SetWindowLongPtr(windowHandle, GWLP_WNDPROC,
            (LONG_PTR)shares_class_window_procedure);
    #else
    originalWindowProc = (WNDPROC)SetWindowLong(windowHandle, GWL_WNDPROC,
            (DWORD)shares_class_window_procedure);
    #endif

    if ( originalWindowProc == NULL )
    {
        DestroyWindow(windowHandle);
        log.Msg(TEXT("SetWindowLong failed."), CLog::Error);
        return false;
    }
    return bRet;
}


LRESULT CALLBACK CWindow::DefProc (
        HWND hWnd,
        UINT uMsg,
        WPARAM wParam,
        LPARAM lParam )
{
    return DefWindowProc ( hWnd, uMsg, wParam, lParam );
}

} // namespace gui

#if FAST_GUI_BUILDING_DLL
BOOL GUI_IMPORT WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    return TRUE;
}
#endif
