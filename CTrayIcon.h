/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CTRAYICON_H_INC
#define CTRAYICON_H_INC

#include <windows.h>

#include "SWC.h"
#include "RES.h"

namespace gui
{

/**
@brief    Базовый класс для управления своими иконками в трее
@details  Позволяет создавать, удалять и даже анимировать иконки в трее,
          добавлять подсказки и прочее.
          пример приложения без интерфейса (только иконка в трее):
@code
          CTrayIcon tray;
          CCP chl(0);
          if (tray.Create())
              tray.Run(0);
@endcode
*/
class GUI_IMPORT CTrayIcon: public CMsgWindow
{
public:
    /**
    @brief    Добавляет иконку в список иконок
    @details  CTrayIcon позволяет менять иконки на лету, но предварительно нужно
              их загрузить в список. Затем выбрать нужную с помощью IconSelect()
              и применить её с помощью SysUpdate() (если иконка ещё не в трее).
    */
    virtual  int IconAdd(HICON hIcon);
    /**
    @brief    Выбирает текущую иконку из списка
    @details  Если ранее иконка была отображена, применяет её сразу с SysUpdate()
    */
    virtual bool IconSelect(int nId);
    /**
    @brief    Выбирает текущую иконку из списка
    @details  Если ранее иконка была отображена, применяет её сразу с SysUpdate()
    */
    virtual bool TipSet(LPCTSTR szTip);
    /**
    @brief    Применяет параметры к системному "Tray"
    */
    virtual bool SysUpdate();
    /**
    @brief    Удаляет иконку из системного "Tray"
    */
    virtual bool SysRemove();
    /**
    @brief    Анимирует иконку nCycles раз по кругу от nFirst до nLast
    @details  если число не положительное то анимация бесконечна.
    */
    virtual bool SysAnimate(int nFirst, int nLast, int nCycles=-1);

    CTrayIcon();
    virtual ~CTrayIcon();

    /**
    @brief    Для обработки наследниками достаточно переопределить эту функцию
    */
    virtual void onTrayEvent(UINT uMsg);
    virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

protected:
    NOTIFYICONDATA iconSettings; //!< собственно настройки иконки
    CImageList images; //!< список иконок доступных к установке
    int selectedIcon; //!< иконка которая установлена сейчас
    int animateCycles; //!< сколько циклов анимации ещё осталось (-1 зациклено)
    int animateReturn; //!< к этой иконке мы вернёмся после анимации
    int animateBegin; //!< используется для повторения циклов
    int animateEnd; //!< окончание анимации
    bool isShowing; //!< true если иконка зарегистрирована в системе
};

} // namespace gui
#endif // CTRAYICON_H_INC
