/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <windows.h>
#include <commctrl.h>
#include <WINGDI.h>

#include "SWC.h"
#include "CLog.h"

#ifndef HWND_MESSAGE
#if(WINVER >= 0x0500)
#define HWND_MESSAGE     ((HWND)-3)
#endif
#endif

namespace gui
{

bool CMsgWindow::Create(CCP& cp)
{
    CCP alter(0);
    alter.set(0,0,0,0,0,0,0,0);
    alter.lpClassName = TEXT("MSGWINDOW");
    alter.hWndParent = HWND_MESSAGE;
    return CCustomWC::Create(alter);
}

LRESULT CMsgWindow::MessageProc( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch ( uMsg )
    {
        case WM_COMMAND:
        {

        } return 0;
    }
    return CWindow::MessageProc(uMsg, wParam, lParam);
}

HWND CMsgWindow::SharedHWND()
{
    static CMsgWindow mwin;
    HWND hwin = mwin;
    if ( hwin == NULL )
    {
        CCP cp(0);
        mwin.Create(cp);
        hwin = mwin;
    }
    return hwin;
}


bool CCustomWC::Create(CCP& cp)
{
    WNDCLASSEX wc;
    wc.cbSize = sizeof(wc);

    if (cp.lpClassName == NULL)
        cp.lpClassName = TEXT("CCustomWC");

    if (GetClassInfoEx(0, cp.lpClassName, &wc) ||
        GetClassInfoEx(GetModuleHandle(0), cp.lpClassName, &wc))
    {
        return CWindow::Create(cp);
    }

    wc.cbSize = sizeof(wc);
    if (cp.hWndParent != HWND_MESSAGE)
    {
        wc.style = CS_DBLCLKS|CS_BYTEALIGNWINDOW|CS_OWNDC;
        wc.hIcon = wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
        wc.hCursor = LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
    }
    else
    {
        wc.style = 0;
        wc.hIcon = 0;
        wc.hIconSm = 0;
        wc.hCursor = 0;
        wc.hbrBackground = (HBRUSH)0;
    }
    wc.hInstance = GetModuleHandle(0);
    wc.lpszClassName = cp.lpClassName;
    wc.lpfnWndProc = DefProc;
    wc.lpszMenuName = 0;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    if (RegisterClassEx(&wc) != 0)
    {
        return CWindow::Create(cp);
    }
    LOG_ERROR(TEXT("error: can't register window class."));
    return false;
}


bool CCustomDlgWC::Create(CCP& cp)
{
    cp.lpClassName = TEXT("CCustomDlgWC");
    return CCustomWC::Create(cp);
}


LRESULT CCustomDlgWC::MessageProc( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch ( uMsg )
    {
        case WM_CLOSE:
        {
            SetModalResult(MODAL_RESULT::CANCEL);
        } return 0;

        case WM_WINDOWPOSCHANGED:
        {
            if (GetWindowLong(handle(), GWL_STYLE) & WS_CHILD)
                break;
            RECT rc;
            if (GetWindowRect(handle(), &rc))
            {
                WPOS loc(0, 0);
                loc.set(rc);
                ReSize(loc);
                return 0;
            }
        } break;

        case WM_ACTIVATE:
        {
            if (LOWORD(wParam) != WA_INACTIVE)
            {
                HWND win = 0;
                int i = 150;
                do {
                    win = FindWindowEx(handle(),win,0,0);
                } while(--i>0 && win && !(GetWindowLong(win, GWL_STYLE) & WS_TABSTOP));
                if (win)
                    SetFocus(win);
                return 0;
            }
        } break;

        case WM_LBUTTONDOWN:
        {
            DWORD dwStyle = GetWindowLong(handle(), GWL_STYLE);
            if (!(dwStyle & WS_THICKFRAME))
                break;
            if (dwStyle & (WS_VSCROLL|WS_HSCROLL))
                break;

            if (GetWindowLong(handle(), GWL_STYLE) & WS_THICKFRAME)
            {
                POINT pt;
                pt.x = LOWORD(lParam);
                pt.y = HIWORD(lParam);
                RECT rc;
                if (GetClientRect(handle(), &rc))
                {
                    rc.left = rc.right - 16;
                    rc.top = rc.bottom - 16;
                    if (PtInRect(&rc, pt))
                    {
                        return CCustomWC::MessageProc(WM_NCLBUTTONDOWN,
                            HTBOTTOMRIGHT, lParam);
                    }
                }
            }
        } break;

        case WM_WINDOWPOSCHANGING:
        {
            DWORD dwStyle = GetWindowLong(handle(), GWL_STYLE);
            if (!(dwStyle & WS_THICKFRAME))
                break;
            if (dwStyle & (WS_VSCROLL|WS_HSCROLL))
                break;

            // подготавливаем отрисовку SIZEGRIP
            if (GetWindowLong(handle(), GWL_STYLE) & WS_THICKFRAME)
            {
                RECT rc;
                if (GetClientRect(handle(), &rc))
                {
                    int ncx = GetSystemMetrics(SM_CXVSCROLL);
                    if (ncx > 0)
                    {
                        rc.left = rc.right - ncx;
                        rc.top = rc.bottom - ncx;
                    }
                    else
                    {
                        rc.left = rc.right - 16;
                        rc.top = rc.bottom - 16;
                    }
                    InvalidateRect(handle(), &rc, TRUE);
                }
            }
        } break;

        case WM_ERASEBKGND:
        {
            DWORD dwStyle = GetWindowLong(handle(), GWL_STYLE);
            if (!(dwStyle & WS_THICKFRAME))
                break;
            if (dwStyle & (WS_VSCROLL|WS_HSCROLL))
                break;

            RECT rc;
            if (!GetClientRect(handle(), &rc))
                break;

            int ncx = GetSystemMetrics(SM_CXVSCROLL);
            if (ncx < 1)
                break;

            HRGN hrgn = CreateRectRgn(rc.left, rc.top, rc.right, rc.bottom);
            if (!hrgn)
                break;
            HRGN dest = CreateRectRgn(0, 0, 0, 0);
            if (!dest)
            {
                DeleteObject(hrgn);
                break;
            }
            HRGN sect = CreateRectRgn(rc.right - ncx, rc.bottom - ncx, rc.right, rc.bottom);
            if (!sect)
            {
                DeleteObject(dest);
                DeleteObject(hrgn);
                break;
            }

            if (dest && sect && hrgn)
            {
                if (CombineRgn(dest, hrgn, sect, RGN_DIFF) == ERROR)
                {
                    DeleteObject(dest);
                    DeleteObject(sect);
                    DeleteObject(hrgn);
                    break;
                }
            }

            FillRgn((HDC)wParam, dest, GetSysColorBrush(COLOR_BTNFACE));

            if (dest) DeleteObject(dest);
            if (sect) DeleteObject(sect);
            if (hrgn) DeleteObject(hrgn);

            rc.left = rc.right - ncx;
            rc.top = rc.bottom - ncx;
            DrawFrameControl((HDC)wParam, &rc, DFC_SCROLL, DFCS_SCROLLSIZEGRIP);
            return TRUE;
        } break;

        case WM_SETCURSOR:
        {
            DWORD dwStyle = GetWindowLong(handle(), GWL_STYLE);
            if (!(dwStyle & WS_THICKFRAME) || handle() != (HWND)wParam ||
                dwStyle & (WS_VSCROLL|WS_HSCROLL))
                break;

            RECT rc;
            if (!GetWindowRect(handle(), &rc))
                break;

            int ncx = GetSystemMetrics(SM_CXVSCROLL);
            if (ncx < 1)
                break;

            rc.left = rc.right - ncx;
            rc.top = rc.bottom - ncx;
            POINT pt;
            DWORD dwPos = GetMessagePos();
            pt.x = LOWORD(dwPos);
            pt.y = HIWORD(dwPos);
            if (PtInRect(&rc, pt))
            {
                HCURSOR cur = LoadCursor(NULL, IDC_SIZENWSE);
                if (cur)
                {
                    SetCursor(cur);
                    return TRUE;
                }
            }
        } break;
    }
    return CCustomWC::MessageProc(uMsg, wParam, lParam);
}


LRESULT CToolBar::MessageProc( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch ( uMsg )
    {
        case WM_RBUTTONDOWN:
        {
            /* TODO (Eugenij#9#): заглушка от залипания кнопок в Виндовс XP */
        } return 0;
    }
    return CWindow::MessageProc(uMsg, wParam, lParam);
}


void CStatusBar::ReSize(WPOS &position)
{
    SNDMSG(handle(), WM_SIZE, 0, MAKELPARAM(position.width(), position.height()));
    RECT rc;
    if (GetWindowRect(handle(), &rc))
    {
        MapWindowPoints(HWND_DESKTOP, GetParent(handle()), (LPPOINT)(&rc), 2);
        position.set(rc);
    }
}


#define SETCBOWNER(h,t) (SetProp(h, TEXT("CBOWNWIN"), (HANDLE)t))
#define GETCBOWNER(h) ((CComboBoxEx*)GetProp(h, TEXT("CBOWNWIN")))
#define REMCBOWNER(h) (RemoveProp(h, TEXT("CBOWNWIN")))


/* Статическая оконная процедура для поля ввода в CComboBoxEx
*/
LRESULT CALLBACK CComboBoxEx::combobox_edit_procedure(
    HWND hWnd,
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam )
{
    CComboBoxEx* owner = GETCBOWNER(hWnd);
    if (owner)
    {
        if (owner->editHandle != hWnd)
            CLog::Instance().Msg(TEXT("owner->editHandle != hWnd."), CLog::Error);
        return owner->EditMessageProc(uMsg, wParam, lParam);
    }
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/* Переопределяемая оконная процедура для поля ввода в CComboBoxEx
*/
LRESULT CComboBoxEx::EditMessageProc(
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam )
{
    if (editOriginalProc && editHandle)
        return CallWindowProc(editOriginalProc, editHandle, uMsg, wParam, lParam);
    return 0;
}


CComboBoxEx::CComboBoxEx()
{
    editOriginalProc = 0;
    editHandle = 0;
}


CComboBoxEx::~CComboBoxEx()
{
    if (editHandle)
    {
        REMCBOWNER(editHandle);
        if (!editOriginalProc)
            return;
        #ifdef GWLP_WNDPROC
        SetWindowLongPtr(editHandle, GWLP_WNDPROC, (LONG_PTR)editOriginalProc);
        #else
        SetWindowLong(editHandle, GWL_WNDPROC, (DWORD)editOriginalProc);
        #endif
        editOriginalProc = 0;
        editHandle = 0;
    }
}



bool CComboBoxEx::Create(CCP& cp)
{
    cp.lpClassName = WC_COMBOBOX;
    if (CWindow::Create(cp))
    {
        CLog& log = CLog::Instance();
        editHandle = FindWindowEx(handle(), 0, TEXT("EDIT"), 0);
        if (!editHandle)
        {
            log.Msg(TEXT("edit field not found."), CLog::Error);
            return false;
        }
        if (!SETCBOWNER(editHandle, this))
        {
            log.Msg(TEXT("edit subclassing fail."), CLog::Error);
            return false;
        }

        #ifdef GWLP_WNDPROC
        editOriginalProc = (WNDPROC)SetWindowLongPtr(editHandle, GWLP_WNDPROC,
        (LONG_PTR)combobox_edit_procedure);
        #else
        editOriginalProc = (WNDPROC)SetWindowLong(editHandle, GWL_WNDPROC,
        (DWORD)combobox_edit_procedure);
        #endif

        if (!editOriginalProc)
        {
            log.Msg(TEXT("edit subclassing fail."), CLog::Error);
            return false;
        }
        return true;
    }
    return false;
}


bool CHotkey::Create(CCP& cp)
{
    cp.lpClassName = HOTKEY_CLASS;
    if (CWindow::Create(cp))
    {
        frameHandle = handle();
        return true;
    }
    return false;
}

} // namespace gui
