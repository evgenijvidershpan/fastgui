/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef IMPORTSDEFS_INC_H
#define IMPORTSDEFS_INC_H

#if FAST_GUI_BUILDING_DLL
    #define GUI_IMPORT __declspec (dllexport)
#else
    #if FAST_GUI_USE_DLL
        #define GUI_IMPORT __declspec (dllimport)
    #else
        #define GUI_IMPORT
    #endif // FAST_GUI_USE_DLL
#endif // FAST_GUI_BUILDING_DLL


namespace gui
{
/**
@brief   Версия библиотеки.

Любой компилятор развернёт эту функцию в константу при статической сборке, в
случае компиляции в dll функция будет внешней.
*/
inline UINT32 GUI_IMPORT version()
{
    return 0x0100;
}

/**
@brief   Класс-пустышка запрещающий копирование своих наследников
*/
class noncopyable {
protected:
    noncopyable(){}
    ~noncopyable(){}
private:
    noncopyable (const noncopyable&);
    noncopyable& operator=(const noncopyable&);
};

}

#endif /* IMPORTSDEFS_INC_H */
