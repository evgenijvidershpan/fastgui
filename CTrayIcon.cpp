/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <windows.h>
#include <commctrl.h>

#include "CTrayIcon.h"
#include "CLog.h"

namespace gui
{

CTrayIcon::CTrayIcon()
{
    RtlZeroMemory(&iconSettings, sizeof(NOTIFYICONDATA));
    iconSettings.cbSize = sizeof(NOTIFYICONDATA);

    LPVOID ppv = (LPVOID) this;
    UINT32* lp = (UINT32*) &ppv;
    iconSettings.uID = *lp;
    if (sizeof(void*) == 8)
        iconSettings.uID ^= *(++lp);

    iconSettings.uCallbackMessage = WM_USER;
    iconSettings.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    iconSettings.uFlags = NIF_ICON|NIF_MESSAGE;
    lstrcpy(iconSettings.szTip, TEXT("Tray icon."));
    selectedIcon = -1;
    isShowing = false;
}


CTrayIcon::~CTrayIcon()
{
    SysRemove();
}


int CTrayIcon::IconAdd(HICON hico)
{
    if (!hico)
        return -1;
    if (!images.handle())
    {
        if (!images.attach(ImageList_Create(16,16,ILC_COLOR32,1,5)))
            return -1;
    }
    return ImageList_AddIcon(images, hico);
}


bool CTrayIcon::IconSelect(int nId)
{
    if (!images.handle())
        return false;

    HICON hico = ImageList_GetIcon(images, nId, ILD_NORMAL);
    if (!hico)
        return false;
    iconSettings.hIcon = hico;
    if (isShowing)
        SysUpdate();
    selectedIcon = nId;
    return true;
}


bool CTrayIcon::TipSet(LPCTSTR szTitle)
{
    if (!szTitle)
        return false;
    lstrcpyn(iconSettings.szTip, szTitle,
             sizeof(iconSettings.szTip)/sizeof(iconSettings.szTip[0]));
    iconSettings.uFlags |= NIF_TIP;
    if (isShowing)
        SysUpdate();
    return true;
}


bool CTrayIcon::SysAnimate(int nFirst, int nLast, int nCycles)
{
    KillTimer(handle(), WM_USER);
    if (!images.handle() || !isShowing)
        return false;
    if (nFirst < 0 || nLast - nFirst < 2)
        return false;

    int nCount = ImageList_GetImageCount(images);
    if (nCount < 2 || nCount <= nLast)
        return false;
    animateReturn = selectedIcon;
    IconSelect(nFirst);
    animateBegin = nFirst;
    animateEnd = nLast;
    animateCycles = nCycles;
    SetTimer(handle(), WM_USER, 333, 0);
    return true;
}


bool CTrayIcon::SysUpdate()
{
    if (!handle())
    {
        CCP cp(0);
        Create(cp);
    }
    if (!IsWindow(handle()))
        return false;

    iconSettings.hWnd = handle();
    if (Shell_NotifyIcon(isShowing? NIM_MODIFY : NIM_ADD, &iconSettings))
    {
        isShowing = true;
        return true;
    }
    return false;
}


bool CTrayIcon::SysRemove()
{
    return Shell_NotifyIcon(NIM_DELETE, &iconSettings) != FALSE;
}


void CTrayIcon::onTrayEvent(UINT uMsg)
{
    if (uMsg != WM_MOUSEMOVE)
    {
        SetModalResult(MODAL_RESULT::CANCEL);
    }
}


LRESULT CTrayIcon::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_USER:
        {
            if (wParam == iconSettings.uID)
                onTrayEvent((UINT) lParam);
        } return 0;

        case WM_TIMER:
        {
            if (wParam != WM_USER)
                break;
            if (selectedIcon < animateEnd)
            {
                IconSelect(selectedIcon+1);
                return 0;
            }
            if (animateCycles > 0)
            {
                if (--animateCycles == 0)
                {
                    IconSelect(animateReturn);
                    animateReturn = -1;
                    KillTimer(handle(), WM_USER);
                    return 0;
                }
            }
            IconSelect(animateBegin);
        } return 0;
    }
    return CMsgWindow::MessageProc(uMsg, wParam, lParam);
}

} // namespace gui
