/* This file is a part of fastgui library.
 Copyright © 2007-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CLOG_H_INC
#define CLOG_H_INC

#include <windows.h>
#include "importdefs.h"

#define WM_LOG_EVENT WM_USER+135

#define LOGGER          (CLog::Instance())
#define LOG_MSG(m)      (LOGGER.Msg(m, CLog::Log))
#define LOG_ERROR(m)    (LOGGER.Msg(m, CLog::Error))
#define LOG_SYSERROR(m) (LOGGER.SysMsg(m, GetLastError(), CLog::Error))

namespace gui
{

/**
@brief    Класс поддержки регистрации событий времени исполнения
@details  CLog реализован в виде синглтона и предоставляет приложению
          возможность централизировано обрабатывать события происходящие
          во время выполнения (например отображать лог действий).
*/
class GUI_IMPORT CLog: noncopyable
{
public:
    enum {
        Warning = -2,
        Error = -1,
        Log = 0
    };
    /**
    @brief    Возвращает общую для приложения ссылку на CLog
    @details  Поскольку CLog реализован в виде синглтона - это единственный
              способ получить доступ к нему.
    */
    static CLog& Instance();
    /**
    @brief    Устанавливает целевое окошко обработчик событий
    @details  Окошко переданное в качестве параметра будет получать
              сообщения WM_LOG_EVENT на каждое событие.
              WPARAM которого будет принимать значение типа сообщения, а
              LPARAM будет указателем на строку
    */
    void SetWindow(HWND hWindow);

    /**
    @brief    Записывает событие в лог
    @details  Способ вывода определяется функциями вида CLog::Set**
              по умолчанию производится вывод на консоль текста из str с
              префиксом зависящим от значения type ("error: ","msg: ","warning")
              если значение type не совпадает с константами CLog::ERROR
              CLog::WARNING или CLog::MSG то выводится префикс "msg(%d):" где
              %d - числовое значение type
    */
    void Msg(LPCTSTR str, int type = CLog::Log);
    /**
    @brief    Записывает событие в лог c добавлением описания ошибки от ОС
    @details  если str == 0 то выводится только описание ошибки от ОС
    */
    void SysMsg(LPCTSTR str, int error, int type = CLog::Error);
private:
    CLog();
    HWND hTarget;
};

} // namespace gui
#endif // CLOG_H_INC
